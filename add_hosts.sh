#!/bin/bash

echo "* Add hosts ..."
sudo echo "192.168.50.2 master" >> /etc/hosts
sudo echo "192.168.50.3 infrastructure" >> /etc/hosts
sudo echo "192.168.50.4 logstash" >> /etc/hosts
sudo echo "192.168.50.5 elasticsearch" >> /etc/hosts
sudo echo "192.168.50.6 kibana" >> /etc/hosts
sudo echo "192.168.50.7 jenkins" >> /etc/hosts
