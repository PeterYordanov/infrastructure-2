#!/bin/bash

echo "* Install BASH Completion ..."
sudo dnf install -y bash-completion

echo "* Add Docker repository ..."
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

echo "* Add the missing dependency ..."
sudo dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.1.el7.x86_64.rpm

echo "* Install Docker ..."
sudo dnf install -y docker-ce docker-ce-cli

echo "* Adjust Docker configuration ..."
sudo mkdir -p /etc/docker
sudo echo '{ "hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"] }' | sudo tee /etc/docker/daemon.json
sudo mkdir -p /etc/systemd/system/docker.service.d/
sudo echo [Service] | sudo tee /etc/systemd/system/docker.service.d/docker.conf
sudo echo ExecStart= | sudo tee -a /etc/systemd/system/docker.service.d/docker.conf
sudo echo ExecStart=/usr/bin/dockerd | sudo tee -a /etc/systemd/system/docker.service.d/docker.conf

echo "* Enable and start Docker ..."
sudo systemctl enable docker
sudo systemctl start docker

echo "* Firewall - open port 2375 ..."
sudo firewall-cmd --add-port=2375/tcp --permanent

echo "* Firewall - set adapter zone ..."
sudo firewall-cmd --add-interface docker0 --zone trusted --permanent

echo "* Firewall - reload rules ..."
sudo firewall-cmd --reload

echo "* Add vagrant user to docker group ..."
sudo usermod -aG docker vagrant

echo "* Install Docker Compose ..."
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
    
echo "* Add BASH Completion for Docker Compose ..."
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.26.2/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

echo "* Run Docker Compose ..."
cd ../..
cd shared_folder
cd shared_folder
docker-compose up