#!/bin/bash

echo "* Download and install the public signing key ..."
sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch

echo "* Install Metricbeat"
sudo yum -y install metricbeat

echo "* Install ElasticSearch ..."
sudo yum install --enablerepo=elasticsearch elasticsearch

systemctl start metricbeat
systemctl enable metricbeat