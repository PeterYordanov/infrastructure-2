#!/bin/bash

echo "* Installing JDK ..."
sudo yum -y install java-1.8.0-openjdk-devel

echo "* Enable Jenkins repository..."
curl --silent --location http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo | sudo tee /etc/yum.repos.d/jenkins.repo

echo "* Import repository ..."
sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key

echo "* Installing Jenkins ..."
sudo yum -y install jenkins

echo "* Starting Jenkins ..."
sudo systemctl start jenkins

echo "* Opening port 8080 ..."
sudo firewall-cmd --permanent --zone=public --add-port=8080/tcp
sudo firewall-cmd --reload

echo "* Enabling Jenkins service ..."
sudo systemctl enable jenkins
systemctl status jenkins

echo "* This is your initial admin password:"
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

echo "* Adding custom user ..."
sudo useradd -d /var/lib/jenkins pyordanov

echo "* Generating ssh key ..."
sudo su - jenkins
sudo ssh-keygen -y -t rsa -C "Jenkins agent key" -f /home/vagrant/.ssh/id_rsa

echo "* Install Docker ..."
sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install -y docker-ce --nobest
sudo usermod -aG docker jenkins

echo "* Start & Enable Docker daemon ..."
sudo service docker start
sudo service docker enable
sudo service jenkins restart
