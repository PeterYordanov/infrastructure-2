#!/bin/bash

echo "* Install Kibana..."
sudo rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
sudo dnf install kibana

echo "* Start Kibana service..."
sudo systemctl start kibana.service
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable kibana.service