class { '::mysql::server':
  root_password           => '12345',
  remove_default_accounts => true,
  restart                 => true, 
  override_options => {
    mysqld => { bind-address => '0.0.0.0'}
  },
}

mysql::db { 'docker_info':
  user        => 'root',
  password    => '12345',
  dbname      => 'docker_info',
  host        => '%',
  sql         => '/vagrant/db/init.sql',
  enforce_sql => true, 
}

class { 'firewalld': }

firewalld_port { 'Open port 3306 in the public zone':
  ensure   => present,
  zone     => 'public',
  port     => 3306,
  protocol => 'tcp',
}

host { 'web':
  ip           => '192.168.50.2',
  host_aliases => 'web',
}

host { 'db':
  ip           => '192.168.50.3',
  host_aliases => 'db',
}
