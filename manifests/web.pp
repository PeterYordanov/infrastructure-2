# Set a global package parameter
Package { ensure => 'installed' }

# List the required packages
$enhancers = [ 'httpd', 'php', 'php-mysqlnd' ]

# Install the required packages
package { $enhancers: }

service { httpd:
  ensure => running,
  enable => true,
}

file { '/var/www/html/index.php':
  ensure => present,
  source => "/vagrant/shared_folder/Dockerfile",
}

class { 'firewalld': }

firewalld_port { 'Open port 80 in the public zone':
  ensure   => present,
  zone     => 'public',
  port     => 80,
  protocol => 'tcp',
}

host { 'web':
  ip           => '192.168.50.2',
  host_aliases => 'web',
}

host { 'db':
  ip           => '192.168.50.3',
  host_aliases => 'db',
}

# Allow Apache to connect over network
selboolean { 'Apache SELinux':
  name       => 'httpd_can_network_connect', 
  persistent => true, 
  provider   => getsetsebool, 
  value      => on, 
}
