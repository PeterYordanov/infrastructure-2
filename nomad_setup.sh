#!/bin/bash

echo "* Initializing script for devops - Nomad Hashicorp"

NOMAD_VERSION=0.7.0
CONSUL_VERSION=1.0.1

sudo mkdir -p /var/wplex/devops
cd /var/wplex/devops

echo "* Fetching consul binary for Linux x64..."
curl -sSL https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip -o consul.zip

echo "* Fetching nomad binary for Linux x64..."
curl -sSL https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip -o nomad.zip

echo "* Fetching nomad binary for LXC containers..."
curl -sSL https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64-lxc.zip -o nomad.zip

echo "* Installing nomad..."
unzip nomad.zip
sudo install nomad /usr/bin/nomad

sudo mkdir -p /etc/nomad.d
sudo chmod a+w /etc/nomad.d

echo "* Installing autocomplete..."
nomad -autocomplete-install

echo "* Creating structure for hashicorp-cluster..."
DATA_DIR="/var/wplex/devops/nomad-hashicorp"

if [ ! -d "$DATA_DIR" ]; then
  sudo mkdir -p "$DATA_DIR"
  echo "Hashicorp directory created: $DATA_DIR"
fi

echo "* Cleaning files..."
sudo rm -rf nomad.zip

echo "* Done script for devops - Nomad Hashicorp"