#!/bin/bash

echo "* Installing Puppet ..."
sudo dnf install -y https://yum.puppet.com/puppet6/puppet6-release-el-8.noarch.rpm
sudo dnf install -y puppet

echo "* Installing Puppet modules..."
puppet module install puppetlabs/mysql
puppet module install puppet-firewalld
sudo cp -vR ~/.puppetlabs/etc/code/modules/ /etc/puppetlabs/code/